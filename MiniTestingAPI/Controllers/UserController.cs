﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using MiniTestingAPI.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace MiniTestingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;

        public UserController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = @"select UserID as id, UserID, UserName, Department, Email, Status, convert(varchar(10),DateOfJoin,120) as DateOfJoin,PhotoFileName,PhotoFileContentType,PhotoFileContent
                             from dbo.MUser";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MiniTestingAPICon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(User user)
        {
            //if(user.PhotoFileContent == null)
            //{
            //    return new JsonResult("Picture Mustn't Empty.");
            //}
            //else
            //{
            string query = @"insert into dbo.MUser ([UserName],[Department],[Email],[Status],[DateOfJoin],[PhotoFileName],[PhotoFileContentType]) values (
                                '" + user.UserName + @"'
                                ,'" + user.Department + @"'
                                ,'" + user.Email + @"'
                                ,'" + user.Status + @"'
                                ,'" + user.DateOfJoin + @"'
                                ,'" + user.PhotoFileName + @"'
                                ,'" + user.PhotoFileContentType + @"'
                                )";

                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("MiniTestingAPICon");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader); ;

                        myReader.Close();
                        myCon.Close();
                    }
                }

                return new JsonResult("Added Successfully");
            //}

        }

        [HttpPut]
        public JsonResult Put(User user)
        {
            string query = @"update dbo.MUser set 
                             UserName = '" + user.UserName + @"' 
                             ,Department = '" + user.Department + @"' 
                             ,Email = '" + user.Email + @"' 
                             ,Status = '" + user.Status + @"' 
                             ,DateOfJoin = '" + user.DateOfJoin + @"' 
                             ,PhotoFileName = '" + user.PhotoFileName + @"' 
                             ,PhotoFileContentType = '" + user.PhotoFileContentType + @"' 
                             where UserID = " + user.UserID;
            
                             //,PhotoFileContent = '" + user.PhotoFileContent + @"'
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MiniTestingAPICon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"delete from dbo.MUser where UserID = " + id;

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MiniTestingAPICon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string FileName = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + FileName;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return new JsonResult(FileName);
            }
            catch (Exception)
            {
                return new JsonResult("anonymous.png");
            }
        }

        [Route("GetAllDepartmentNames")]
        public JsonResult GetAllDepartmentNames()
        {
            string query = @"select DepartmentId as id, DepartmentName from dbo.Department";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MiniTestingAPICon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

    }
}
