﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniTestingAPI.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public Boolean Status { get; set; }
        public DateTime DateOfJoin { get; set; }
        public string PhotoFileName { get; set; }
        public string PhotoFileContentType { get; set; }
        public Byte[] PhotoFileContent { get; set; }
    }
}
